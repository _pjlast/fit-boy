const colorSet = [
  {color: "#ffb642"},
  {color: "#1AFF80"},
  {color: "#2ecfff"},
  {color: "#c0ffff"}
];

const options = [
  ['Fit-boy color', 'color']
];

function mySettings(props) {
  return (
    <Page>
      {options.map(([title, settingsKey]) =>
        <Section
          title={title}>
          <ColorSelect
            settingsKey={settingsKey}
            colors={colorSet} />
        </Section>
      )}
    </Page>
  );
}

registerSettingsPage(mySettings);